vim.g.gitblame_enabled = 0
vim.keymap.set("n", "<Leader>gb", ":GitBlameToggle<CR>")
return { "f-person/git-blame.nvim" }
