return {
  "nvim-orgmode/orgmode",
  dependencies = {
    { "nvim-treesitter/nvim-treesitter", lazy = true },
  },
  event = "VeryLazy",
  config = function()
    -- Load treesitter grammar for org
    --require("orgmode").setup_ts_grammar()
    --

    -- Setup orgmode
    require("orgmode").setup({
      org_agenda_files = "~/Dev/orgfiles/**/*",
      org_default_notes_file = "~/Dev/orgfiles/refile.org",
      org_log_done = "note",
      org_log_into_drawer = "LOGBOOK",
    })
    -- NOTE: (from docs) If you are using nvim-treesitter with `ensure_installed = "all"` option
    -- add `org` to ignore_install
    require('nvim-treesitter.configs').setup({
      ensure_installed = 'all',
      ignore_install = { 'org' },
    })
  end,
}
