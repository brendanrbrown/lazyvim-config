return {
  "williamboman/mason.nvim",
  opts = {
    ensure_installed = {
      "lua-language-server",
      "stylua",
      "json-lsp",
      "shfmt",
      "yaml-language-server",
      --"yamlfmt",
      --"yamllint",
      "jedi-language-server",
      "sqlls",
      --"purescript-language-server",
    },
  },
}
