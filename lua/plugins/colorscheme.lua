return {
  { "nyoom-engineering/oxocarbon.nvim" },
  { "savq/melange-nvim" },
  {
    "rebelot/kanagawa.nvim",
    theme = "wave",
    background = {
      dark = "dragon",
      light = "lotus",
    },
  },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "kanagawa",
    },
  },
}
