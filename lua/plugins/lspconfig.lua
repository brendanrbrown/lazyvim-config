return {
  "neovim/nvim-lspconfig",
  opts = {
    -- https://discourse.purescript.org/t/purescript-setup-with-neovim-lazyvim/3491
    setup = {
      purescriptls = function(_, opts)
        opts.root_dir = function(path)
          local util = require("lspconfig.util")
          if path:match("/.spago/") then
            return nil
          end
          return util.root_pattern("spago.dhall")(path)
        end
      end,
    },
    servers = {
      jedi_language_server = {},
      lua_ls = {},
      rust_analyzer = {},
      hls = {
        -- TODO: allow multiple commands?
        -- sometimes you need ...-wrapper
        cmd = { "haskell-language-server", "--lsp" },
        settings = {
          haskell = {
            formattingProvider = "fourmolu",
            plugin = { fourmolu = { config = { external = true } } },
          },
        },
        mason = false,
      },
      purescriptls = {
        mason = false,
        --root_pattern = "./**/spago.dhall",
        settings = {
          --on_attach = true,
          purescript = {
            formatter = "purs-tidy",
            addSpagoSources = true,
          },
          flags = {
            debounce_text_changes = 150,
          },
        },
      },
      ocamllsp = { mason = false },
      sqlls = {
        mason = true,
        settings = {
          connections = {
            {
              name = "cardano-db-sync",
              adapter = "postgresql",
              host = "0.0.0.0",
              port = 5432,
              user = "brb",
              password = "",
              database = "cexplorer",
              projectPaths = { "/home/brb/Dev/external/psql" },
            },
          },
        },
      },
    },
  },
}
