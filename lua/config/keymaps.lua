-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

vim.keymap.set("i", "__", "<- ")
vim.keymap.set("i", "_+", "-> ")
vim.keymap.set("i", ")+", "=> ")
vim.keymap.set("n", "oo", "o<esc>")
vim.keymap.set("n", "OO", "O<esc>")
