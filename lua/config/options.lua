-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

-- THEME
vim.opt.background = "light"

-- LEADER
vim.g.mapleader = "\\"
vim.g.maplocalleader = ","

-- TEXT
vim.g.textwidth = 80
vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"

-- OTHER
-- Needed for purescript
vim.filetype.add({ extension = { purs = 'purescript' }})
vim.g.autoformat = false
